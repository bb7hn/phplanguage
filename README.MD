# PHP Language Package
This package is a simple language class for PHP that allows you to easily manage translations for your project.


## Installation
You can install it by composer
```
    $ composer require batuhanozen/php-language
```
```php
    use use PhpLanguage\Language;
    $translate = new Language('./languages', 'en', '.');
```
or to use this package, you need to include the PhpLanguage.php file in your project and create an instance of the class by passing in the path of your language files, the language you want to use, and the separator to use for the keys in your language files.

```php
    require_once 'PhpLanguage.php';
    $translate = new Language('./languages', 'en', '.');
```

## Methods
__construct($language_files_path, $language, $key_separator)
This is the constructor for the class. It takes in three parameters:

`$language_files_path`: the path of the language files.
`$language`: the language code (en, tr, fr etc.).
`$key_separator`: the separator for the keys in the language file.
set_path($path)
This method sets the path for the language files. It takes in one parameter:

`$path`: the path for the language files.
set_language($language)
This method sets the language for the translations. It takes in one parameter:

`$language`: the language code (en, tr, fr etc.).
translate($key, $variables)
This method translate the given key and replace the given variables in the translation text. It takes in two parameters:

`$key`: the key for the translation.
`$variables`: the variables to replace in the translation.
Usage
You can use the translate method to get the translations for a specific key. The key should be in the format key1.key2.key3 if you are using a separator of `.`.
```php
$translation = $translate->t('home.title');
```
You can also replace placeholders in the translation text by passing in an associative array of variables.
```php
$translation = $translate->t('home.welcome', ['{name}' => 'John']);

```

## Conclusion
This package provides a simple way to manage translations in your PHP projects. You can easily change the language, path of the language files or key separator and you can also replace placeholders in the translation text.

# Contributing
I welcome contributions to this project. If you are interested in contributing, please follow these guidelines:

Fork the repository and make your changes in a separate branch.
Test your changes to ensure they work as intended.
Submit a pull request, including a detailed description of your changes and why you think they should be included.
I will review your pull request and may request additional changes before merging it.

Please note that by contributing to this project, you agree to abide by our code of conduct.