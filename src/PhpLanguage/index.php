<?php

namespace PhpLanguage;

use Exception;
use InvalidArgumentException;

class Language
{
    private $BASE = '';
    private $path = './languages';
    private $language = 'en';
    private $options = ['separator' => '.'];
    private $translations = [];

    /**
     * Constructor for the class
     * @param string $language_files_path Path of the language files
     * @param string $language Language code (en, tr, fr etc.)
     * @param string $key_separator Separator for the keys in the language file
     */
    public function __construct($language_files_path='languages', $language='en', $key_separator='.')
    {
        $base_dir = dirname(dirname(dirname(dirname(dirname(__DIR__)))));

        $this->BASE = $base_dir ? $base_dir.'/' : '';
        $this->path = $language_files_path ?: $base_dir.$language_files_path;
        $this->language = $language;
        $this->options['separator'] = $key_separator;

        // Get the language file path
        $language_file_path = $this->path."/$this->language.json";

        // Throw an exception if the language file path doesn't exist
        if (!file_exists($language_file_path)) {
            throw new Exception("$language_file_path could not be found.");
        }

        // Set the translations from the language file
        $this->translations = json_decode(file_get_contents($language_file_path), true);
    }
    /**
     * Set the path for the language files
     * @param string $path Path for the language files
     * @return string Returns the path set
     */
    public function set_path($path)
    {
        // Throw an exception if path is not a string
        if (!is_string($path)) {
            throw new InvalidArgumentException("Path have to be string");
        }

        $this->path = $this->BASE.$path;

        return $this->path;
    }
    /**
     * Set the language for the translations
     * @param string $language Language code (en, tr, fr etc.)
     * @return bool Returns true if the language is set successfully, false otherwise
     */
    public function set_language($language)
    {
        // Throw an exception if the language is not a string
        if (!is_string($language)) {
            throw new InvalidArgumentException("Language  should be a string as language code (en, tr, fr etc.)");
        }
        $this->language = $language;

        // Get the language file path
        $language_file_path = $this->BASE."$this->path/$this->language.json";

        // Set the translations accordingly if the language file exists
        if (file_exists($language_file_path)) {
            $this->translations = json_decode(file_get_contents($language_file_path), true);
            return true;
        }

        return false;
    }
     /**
     * Translate the given key and replace the given variables in the translation text
     * @param string $key Key for the translation
     * @param array $variables Variables to replace in the translation
     * @return string Returns the translated string
     */
    public function t($key = '', $variables = [])
    {
        // Return empty string if no key is provided
        if (!$key) {
            return '';
        }

        // Throw an exception if key is not a string
        if (!is_string($key)) {
            throw new InvalidArgumentException('Key should be a string');
        }

        // Throw an exception if the separator is not a string
        if (!is_string($this->options['separator'])) {
            throw new InvalidArgumentException('Separator should be a string');
        }

        // Use array_reduce to reduce the number of iterations
        // to find the correct key in the translation array
        $result = array_reduce(explode($this->options['separator'], $key), function ($carry, $key) {
            return $carry[$key] ?? "missing translation '$key'";
        }, $this->translations);

        // Replace variables in translation
        foreach ($variables as $key => $var) {
            $result = str_replace($key, $var, $result);
        }

        // Return the result of the translation
        return $result;
    }
}
